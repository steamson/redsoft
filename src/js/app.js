import { TweenMax, Linear } from 'gsap/all'
import ScrollMagic from 'scrollmagic'

import '../sass/app.sass'

import 'animation.gsap'

let addEvent = function(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return
    if (object.addEventListener) object.addEventListener(type, callback, false)
    else if (object.attachEvent) object.attachEvent("on" + type, callback)
    else object["on"+type] = callback
}

$(document).ready(function() {
    
    // =========================================================================
    // ON SCROLL MENU ACTIVATOR ================================================
    // =========================================================================

    let offset = $('body').offset(), scroll_start

    function stickyMenu() {
        scroll_start = $(document).scrollTop()

        if(scroll_start > offset.top)
            $('header').addClass('scroll')
        else
            $('header').removeClass('scroll')
    }

    addEvent(window, 'scroll', function () { stickyMenu() })

    stickyMenu()

    // =========================================================================
    // SCROLL ANIMATIONS =======================================================
    // =========================================================================

    let anime = new ScrollMagic.Controller({ globalSceneOptions: {
        triggerHook: 1
        // duration: 100
    }})

    $('[data-anime]').each(function () {
        let animation = "fadeIn"
        if ( $(this).attr('data-anime').length > 0 ) animation = $(this).attr('data-anime')

        let delay = $(this).attr('data-delay')
        if (delay) if ( delay.length > 0 ) delay = parseFloat( $(this).attr('data-delay') )

        let tween
        let time = 0.5
        if ( typeof $(this).attr('data-time') != 'undefined' )
            if ( $(this).attr('data-time').length > 0 )
                time = parseFloat( $(this).attr('data-time') )

        switch (animation) {
            case 'fadeIn':
                tween = TweenMax.to( $(this), time, { autoAlpha: 1, delay: delay, ease:Linear.ease } )
                break

            case 'fadeInScale':
                tween = TweenMax.to( $(this), time, { autoAlpha: 1, scale: 1, delay: delay, ease:Linear.ease })
                break

            case 'fadeInLeft':
                tween = TweenMax.to( $(this), time, { autoAlpha: 1, x: '=0', delay: delay, ease:Linear.ease } )
                break

            case 'fadeInRight':
                tween = TweenMax.to( $(this), time, { autoAlpha: 1, x: '=0', delay: delay, ease:Linear.ease } )
                break

            case 'fadeInUp':
                tween = TweenMax.to( $(this), time, { autoAlpha: 1, y: '=0', delay: delay, ease:Linear.ease } )
                break

            case 'fadeInUpScale':
                tween = TweenMax.to( $(this), time, { autoAlpha: 1, scale: 1, y: '=0', delay: delay, ease:Linear.ease } )
                break

            case 'fadeInDown':
                tween = TweenMax.to( $(this), time, { autoAlpha: 1, y: '=0', delay: delay, ease:Linear.ease } )
                break

            case 'menu':
                tween = TweenMax.to( $(this), time, { autoAlpha: 1, y: '=0', delay: delay, ease:Linear.ease } )
                break
        }

        new ScrollMagic.Scene({ triggerElement: this }).setTween(tween).addTo(anime)
    })
})