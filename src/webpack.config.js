const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const globImporter = require('node-sass-glob-importer')
const webpack = require('webpack')
const path = require('path')
const fs = require('fs-extra')

function generateHtmlPlugins (templateDir) {
    const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir))
    return templateFiles.map(item => {

        const parts = item.split('.')
        const name = parts[0]
        const extension = parts[1]

        var pathIn = '../pages'

        if ( name == 'index' ) pathIn = '..'

        return new HtmlWebpackPlugin({
            filename: `${pathIn}/${name}.html`,
            template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`)
        })
    })
}

const htmlPlugins = generateHtmlPlugins('./pug')

module.exports = {
  entry: {
    script: './js/app.js',
  },

  output: {
    filename: '../script.js',
    path: path.resolve(__dirname, '../public/components'),
  },

  module: {
    rules: [
      {
        test: /\.(pug|jade)$/,
        loaders: ['html-loader', 'pug-html-loader'],
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.ico$|\.mp3$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            outputPath: '../components',
            publicPath: (url) => { return `components/${url}`; },
          },
        }]
      },
      {
        test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: '/fonts',
            publicPath: 'components/fonts',
            useRelativePath: false
          }
        }]
      },
      {
        test: /\.s(a|c)ss$/,
        use: ExtractTextPlugin.extract([
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              importer: globImporter()
            }
          }
        ])
      }
    ]
  },
  resolve: {
      alias: {
          "TweenMax": path.resolve('node_modules', 'gsap/src/uncompressed/TweenMax.js'),
          "TweenLite": path.resolve('node_modules', 'gsap/src/uncompressed/TweenLite.js'),
          "TimelineMax": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineMax.js'),
          "TimelineLite": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineLite.js'),
          "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
          "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js')
      }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      'window.jQuery': 'jquery',
    }),
    new ExtractTextPlugin('../style.css')
  ].concat(htmlPlugins)
}